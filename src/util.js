export function formatComponentName (name) {
  return name.toLowerCase() + '.json'
}

export function printError (error, debugInfo, expand) {
  let location = debugInfo.location ? ' on ' + debugInfo.location : ''
  let line = debugInfo.line ? ' at line ' + (debugInfo.line) : ''
  let when = debugInfo.when ? ' [' + debugInfo.when + ']' : ''

  if (!expand) {
    console.groupCollapsed('Error in ' + formatComponentName(debugInfo.component) + location + line + when)
  } else {
    console.group('Error in ' + formatComponentName(debugInfo.component) + location + line + when)
  }

  if (debugInfo.exp) {
    console.log(debugInfo.exp)
  }
  if (debugInfo.hint) {
    console.log(debugInfo.hint)
  }
  console.error(error)
  console.groupEnd()
}
