import Vue from 'vue'
import VueResource from 'vue-resource'
import { loadlibs } from './lib-loader'
import { formatComponentName, printError } from './util'
import ExpRunner from './exp-runner'
import { isObject, isEmpty, map } from 'lodash/core'
import parseJSON from 'parse-json'

const COMPONENT_FILE_PATH = window.PLANSYS_FILE_PATH || 'static/{component}.jsontxt'
const COMPONENT_DIR_PATH = window.PLANSYS_DIR_PATH || 'static/{component}/'
const COMPONENT_LIB_PATH = window.PLANSYS_LIB_PATH || 'static/{component}/libs/{lib}/{file}'

const COMPONENT_DEFAULT_CONTEXT = {
  name: '',
  libs: {},
  init: {},
  vms: {} // vue's vm instances of this component
}

// use vue resource
Vue.use(VueResource)

export default class Component {
  static Vue = Vue
  static state = {
    page: {}
  }

  static init (el, componentName) {
    let initializer = new Component(componentName, true)

    return initializer.then(context => {
      return new Promise(resolve => {
        if (context) {
          resolve(new Vue({
            el: el,
            template: '<' + componentName + '></' + componentName + '>'
          }))
        } else {
          resolve(false)
        }
      })
    })
  }

  updateTemplate (template) {
    this.initParams.template = this.context.init.template
  }

  constructor (componentName, isRoot = false) {
    this.context = null
    this.initParams = null

    return new Promise(resolve => {
      var url = COMPONENT_FILE_PATH.replace('{component}', componentName)
      Vue.http.get(url).then((res) => {
        let compileComponent = () => {
          this.initContext(componentName, res.body)
          .then(res => { return this.loadLibraries() })
          .then(res => { return this.createComponent(isRoot) })
          .then(res => {
            resolve(res)
          })
        }

        let parseBody = (val) => {
          try {
            let body = parseJSON(val)
            if (body) {
              res.body = body
              compileComponent()
            }
          } catch (err) {
            console.log(err)
            // console.group('Invalid ' + formatComponentName(componentName) + ' component: ')
            // console.error(err)
            // console.groupEnd()
          }
        }

        if (!res.body && res.bodyText) {
          res.bodyText.then(val => {
            parseBody(val)
          })
          resolve(false)
          return false
        } else if (res.body.constructor === window.Blob) {
          let reader = new window.FileReader()
          reader.readAsText(res.body)
          reader.onloadend = () => {
            parseBody(reader.result)
          }
        } else {
          compileComponent()
        }
      }, (res) => {
        resolve(false)
      })
    })
  }

  initContext (componentName, body) {
    let context = JSON.parse(JSON.stringify(COMPONENT_DEFAULT_CONTEXT))
    this.context = context
    return new Promise(resolve => { // eslint-disable-line
      if (!body.name) {
        printError('missing name field in json', {
          component: componentName,
          hint: 'Please define component name in json'
        })
        return false
      }

      if (!body.init) {
        printError('missing init field in json', {
          component: componentName,
          hint: 'Please define init field in json'
        })
        return false
      } else {
        if (!body.init.template && !body.init.templateFile) {
          printError('missing init:template or init:templateFile field in json', {
            component: componentName,
            hint: 'Please define init:template or init:templateFile field in json'
          })
          return false
        }
      }

      context.name = body.name
      context.init = body.init
      context.on = body.on || {}

      if (body.init.templateFile && !body.init.template) {
        var templateUrl = COMPONENT_DIR_PATH.replace('{component}', componentName) + body.init.templateFile
        Vue.http.get(templateUrl).then(r => {
          context.init.template = r.body.split('<!-- EOT -->')[0]
          resolve()
        }).catch(() => {
          printError('Failed to get templateFile: ' + templateUrl, {
            component: componentName
          })
        })
      } else {
        resolve()
      }
    })
  }

  loadLibraries () {
    let context = this.context
    return new Promise(resolve => {
      // determine if we should load external libraries
      let libs = context.init.libs || []
      var shouldLoadLibs = false
      libs.forEach(lib => {
        if (!context.libs[lib[0]]) {
          context.libs[lib[0]] = []
          shouldLoadLibs = true
        }
      })

      if (shouldLoadLibs) {
        // convert each file path to correct path
        libs.forEach(lib => {
          context.libs[lib[0]] = {
            all: lib[1],
            loaded: []
          }

          lib[1].forEach((file, key) => {
            lib[1][key] = COMPONENT_LIB_PATH
            lib[1][key] = lib[1][key].replace('{component}', context.name)
            lib[1][key] = lib[1][key].replace('{lib}', lib[0])
            lib[1][key] = lib[1][key].replace('{file}', file)
          })
        })

        // load all libraries dependencies
        loadlibs(context, libs, (lib, file) => {
          context.libs[lib].loaded.push(file)
        }, () => {
          resolve()
        })
      } else {
        // we dont need to load external libs
        resolve()
      }
    })
  }

  createComponent (isRoot) {
    let context = this.context
    let exprun = new ExpRunner(context)
    let initParams = {
      template: JSON.parse(JSON.stringify(context.init.template))
    }

    this.initParams = initParams
    return new Promise(resolve => {
      // bind context event
      if (!isEmpty(context.on) && isObject(context.on)) {
        map(context.on, (funcarr, funcname) => {
          let funcdef = exprun.defineFunc(funcarr, {
            component: context.name,
            location: 'event:' + funcname
          })
          initParams[funcname] = function () {
            return funcdef(this)
          }
        })

        let onBeforeCreate = initParams.beforeCreate || new Function () // eslint-disable-line
        initParams.beforeCreate = function () {
          context.vms[this._uid] = this
          onBeforeCreate()
        }
      }

      // define illegal varname
      let illegalVarName = ['state', 'context', 'page', 'this', 'component']
      let isVarValid = (obj, location, vars) => {
        var valid = true
        vars.forEach(varname => {
          if (typeof obj[varname] !== 'undefined') {
            printError('Local variable name can not be `' + varname + '` ', {
              component: context.name,
              location: location + '.' + varname,
              hint: 'Please change `' + varname + '` variable to another name'
            })
            valid = false
          }
        })
        return valid
      }

      // bind context data
      if (isObject(context.init.data)) {
        if (!isEmpty(context.init.data.local)) {
          if (!isVarValid(context.init.data.local, 'context.init.data.local', illegalVarName)) {
            return false
          }
        }

        if (isRoot) {
          if (context.init.data.state) {
            Component.state = context.init.data.state
          }
        }

        initParams.data = () => {
          let data = JSON.parse(JSON.stringify(context.init.data.local))

          if (isObject(context.init.data.component)) {
            data.component = JSON.parse(JSON.stringify(context.init.data.component))
          }
          data.state = Component.state
          return data
        }

        if (!isEmpty(context.init.data.public)) {
          initParams.props = JSON.parse(JSON.stringify(context.init.data.public))

          if (!isVarValid(context.init.data.public, 'context.init.data.public', illegalVarName)) {
            return false
          }
        }
      }

      // bind context var
      initParams.computed = {
        context: () => {
          return context
        },
        page: () => {
          return Component.state.page
        }
      }

      if (!isEmpty(context.init.methods) && isObject(context.init.methods)) {
        initParams.methods = {}

        map(context.init.methods, (funcarr, funcname) => {
          let funcdef = exprun.defineFunc(funcarr, {
            component: context.name,
            location: 'method:' + funcname
          })
          initParams.methods[funcname] = function () {
            return funcdef(this)
          }
        })
      }

      if (context.init.components && context.init.components.length > 0) {
        // chain load component dependencies
        var componentLoader = false
        context.init.components.forEach((com, idx) => {
          if (!componentLoader) {
            componentLoader = new Component(com)
          } else {
            componentLoader = componentLoader.then(result => {
              componentLoader = new Component(com)
            })
          }
        })

        // after all dependencies loaded, then load current component
        componentLoader.then(result => {
          Vue.component(context.name, initParams)
          resolve(context)
        })
      } else {
        Vue.component(context.name, initParams)
        resolve(context)
      }
    })
  }
}
