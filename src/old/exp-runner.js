import { printError } from './util'
import { isArray, isEmpty } from 'lodash/core'

export default class Exp {

  constructor (context) {
    this.context = context
  }

  defineFunc (exps, debugInfo) {
    let debugInfoLine = []
    let funcs = []

    for (var line in exps) {
      debugInfoLine[line] = {
        component: debugInfo.component,
        location: debugInfo.location,
        line: parseInt(line) + 1,
        exp: 'Expression: [' + (isArray(exps[line]) ? exps[line].join('] [') : exps[line]) + ']',
        when: 'Declaration'
      }
      var exp = this.compileExp(exps[line], debugInfoLine[line])
      if (exp) {
        funcs.push(exp)
      } else {
        break
      }
    }

    return (self) => {
      var lastResult = null
      var error = false

      if (!isEmpty(funcs)) {
        let executor = funcs[0](self).then(result => {
          if (typeof result !== 'undefined') {
            lastResult = result
          }

          funcs.forEach((func, line) => {
            if (line > 0 && !error) {
              executor = executor
              .then(result => {
                if (typeof result !== 'undefined') {
                  lastResult = result
                }
                return func(self, lastResult)
              })
              .catch(error => {
                printError(error, debugInfoLine[line])
                error = true
              })
            }
          })

          return new Promise(resolve => {
            resolve(lastResult)
          })
        }).catch(error => {
          printError(error, debugInfoLine[0])
          error = true
        })

        return executor
      }

      return false
    }
  }

  compileExp (exp, debugInfo) {
    if (isArray(exp)) {
      if (exp[0].substr(0, 2) !== '//') { // if first array is not comment
        if (exp.length > 1) {
          return this.parseExp(exp[0], exp.slice(1), debugInfo)
        } else {
          return this.parseExp('eval', [exp[0]], debugInfo)
        }
      }
    }
  }

  parseExp (name, params, debugInfo) {
    let expr = {
      wait: [
        ['Wait for `secs` seconds then continue to next line'],
        ['int'],
        function (secs, resolve, reject, lastResult) {
          setTimeout(e => {
            resolve()
          }, secs * 1000)
        }
      ],
      log: [
        ['Print log data'],
        ['expr'],
        function (data, resolve, reject, lastResult) {
          try {
            console.log(new Function('lastResult', ' return ' + data).bind(this)(lastResult)) // eslint-disable-line  
          } catch (e) {
            reject(e)
          } finally {
            resolve()
          }
        }
      ],
      eval: [
        ['Evaluate expression then store result to `lastResult` variable'],
        ['expr'],
        function (data, resolve, reject, lastResult) {
          var result
          try {
            result = new Function('lastResult', ' return ' + data).bind(this)(lastResult) // eslint-disable-line
          } catch (e) {
            reject(e)
          } finally {
            resolve(result)
          }
        }
      ],
      geturl: [
        ['Get content from source url then store it to `lastResult` OR target variable if supplied'],
        ['expr', 'str='],
        function (url, varname, resolve, reject, lastResult) {
          this.$http.get(url).then(res => {
            res.bodyText.then(text => {
              if (!isEmpty(res.body)) {
                if (varname !== '') {
                  let setVarName = varname + ' = ' + JSON.stringify(res.body)
                  resolve(new Function('lastResult', setVarName).bind(this)(lastResult)) // eslint-disable-line
                }
                resolve(res.body)
              } else {
                let contentType = res.headers.map['Content-Type']
                if (!isEmpty(contentType)) {
                  if (contentType[0].indexOf('json') >= 0) {
                    res.bodyText.then(text => {
                      reject('Invalid JSON response:\n' + text)
                    })
                  } else {
                    resolve(res.body)
                  }
                }
              }
            })
          }).catch(res => {
            reject(res.body.trim() + ' ' + res.status + ' (' + res.statusText + ')')
          })
        }
      ],
      set: [
        ['Set variable with value'],
        ['str', 'str'],
        function (varname, value, resolve, reject, lastResult) {
          try {
            let setVarName = varname + ' = ' + value
            resolve(new Function('lastResult', setVarName).bind(this)(lastResult)) // eslint-disable-line
          } catch (e) {
            reject(e)
          } finally {
            resolve()
          }
        }
      ],
      call: [
        ['Call a method'],
        ['str', 'array=[]'],
        function (method, params, resolve, reject, lastResult) {
          new Function('resolve', 'lastResult', // eslint-disable-line
           method + `.apply(this).then(res => {
            resolve(res);
          })`).bind(this)(resolve, lastResult)
        }
      ]
    }

    if (isEmpty(expr[name])) {
      return false
    }

    return this.parseParams(expr[name][0], expr[name][1], expr[name][2], params, name, debugInfo)
  }

  parseParams (docs, rules, callback, params, name, debugInfo) {
    if (params.length < rules.length) {
      // determine default value
      rules.forEach((rule, i) => {
        let defaultVal = rule.split('=')
        if (defaultVal.length > 1) {
          switch (defaultVal[0].trim()) {
            case 'expr':
            case 'array':
              params[i] = eval(defaultVal[1]) // eslint-disable-line
              break
            case 'str':
              params[i] = defaultVal[1]
          }
        }
      })

      if (params.length < rules.length) {
        let msg = 'insufficient parameter (expected:' + (rules.length + 1) + ', found:' + (params.length + 1) + ')'
        debugInfo.hint = 'Hint      : [' + name + '] [' + rules.join('] [') + ']'
        printError(msg, debugInfo, true)
        return false
      }
    }

    return (self, lastResult) => {
      return new Promise((resolve, reject) => {
        params.push(resolve)
        params.push(reject)
        params.push(lastResult)
        callback.apply(self, params)
      })
    }
  }
}
