import Component from './component'
import _ from 'lodash/core'

if (!_.isObject(window.plansys)) {
  window.plansys = {
    app: 'builder',
    path: {}
  }
} else if (!window.plansys.path) {
  window.plansys = {
    app: 'builder',
    path: {}
  }
}

Component.init('#app', window.plansys.app || 'builder')
