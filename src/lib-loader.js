import { printError } from './util'

let firstScriptEl = document.getElementsByTagName('script')[0]

export function loadlibs (context, libs, progress, done) {
  var loader = false
  let fileLoaded = file => {
    progress(lib[0], file)
  }

  for (var lib of libs) {
    let files = lib[1]

    if (files.length > 0) {
      loader = loadfile(files[0], {
        component: context.name,
        location: 'libs:' + lib[0] + ':' + files[0],
        line: 1
      })

      files.forEach((file, i) => {
        if (i > 0) {
          loader = loader.then(loadedFile => {
            fileLoaded(loadedFile)
            return loadfile(files[i], {
              component: context.name,
              location: 'libs:' + lib[0] + ':' + files[i],
              line: i + 1
            })
          })
        }
      })

      loader.then(loadedFile => {
        fileLoaded(loadedFile)
        done()
      })
    } else {
      done()
    }
  }
}

export function loadfile (file, debugInfo) {
  let ext = file.split('.').pop()
  var loadPromise = false

  switch (ext) {
    case 'css':
      loadPromise = loadcss(file, debugInfo)
      break
    default:
      loadPromise = loadjs(file, debugInfo)
      break
  }

  return loadPromise
}

export function loadjs (file, debugInfo) {
  return new Promise((resolve) => {
    var el = document.createElement('script')
    el.type = 'text/javascript'
    el.async = false
    el.src = file

    el.addEventListener('load', e => {
      resolve(file, e)
    }, false)

    el.addEventListener('error', e => {
      resolve(file, e)
      printError('Failed to load ' + file, debugInfo)
    }, false)

    firstScriptEl.parentNode.insertBefore(el, firstScriptEl)
  })
}

export function loadcss (file, debugInfo) {
  return new Promise((resolve) => {
    var head = document.getElementsByTagName('head')[0]
    var el = document.createElement('link')
    el.rel = 'stylesheet'
    el.type = 'text/css'
    el.href = file
    el.media = 'all'

    el.addEventListener('load', e => {
      resolve(file, e)
    }, false)

    el.addEventListener('error', e => {
      resolve(file, e)
      printError('Failed to load ' + file, debugInfo)
    }, false)

    head.appendChild(el)
  })
}
