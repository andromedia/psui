import Vue from 'vue'
import Vuex from 'vuex'
import VueResource from 'vue-resource'
import { loadlibs } from './lib-loader'
import { formatComponentName, printError } from './util'
import _ from 'lodash/core'
import Hjson from 'hjson'

const COMPONENT_ROOT = window.plansys.path.root || 'static/'
const COMPONENT_CTRL = window.plansys.path.ctrl || ''
const COMPONENT_HTML_PATH = window.plansys.path.html || COMPONENT_ROOT + '{component}.html'
const COMPONENT_JSON_PATH = window.plansys.path.json || COMPONENT_ROOT + '{component}.hjson'
const COMPONENT_LIB_PATH = window.plansys.path.lib || COMPONENT_ROOT + '{component}/_libs/{lib}/{file}'
const COMPONENT_DEFAULT_CONTEXT = {
  name: '',
  libs: {}, // to store loaded libs
  require: {}, // to store required libs and components
  ctrl: {}
}

// use vue resource
Vue.use(VueResource)

// use vuex
Vue.use(Vuex)

export default class Component {
  static Vue = Vue
  static vms = { // to store all child vms instance
    byName: {},
    byUID: {}
  }
  static defs = {} // to store component definitions
  static store = {}

  /**
   * init ini dipakai untuk meng-instantiate root component
   */
  static init (el, componentName) {
    // instantiate component root nya
    let initializer = new Component(componentName, true)

    // lalu tempelkan component tersebut ke vue instance yg pertama
    return initializer.then(context => {
      return new Promise(resolve => {
        if (context) {
          resolve(new Vue({
            el: el,
            template: '<' + componentName + '></' + componentName + '>'
          }))
        } else {
          resolve(false)
        }
      })
    })
  }

  /**
   * menginstantiate component, returnnya promise
   */
  constructor (componentName, isRoot = false) {
    this.context = null
    this.initParams = null
    this.componentPath = null
    this.templateUrl = null

    return new Promise(resolve => {
      var url = ''
      if (_.isArray(componentName)) {
        // if componentName is object then separate url and name
        url = COMPONENT_JSON_PATH.replace('{component}', componentName[0])
        this.templateUrl = COMPONENT_HTML_PATH.replace('{component}', componentName[0])
        componentName = componentName[1]
      } else {
        // ambil component sesuai dengan path yg ditentukan di COMPONENT_JSON_PATH
        url = COMPONENT_JSON_PATH.replace('{component}', componentName)
        this.templateUrl = COMPONENT_HTML_PATH.replace('{component}', componentName)
      }

      this.componentPath = componentName

      // get last part of url
      componentName = componentName.split('/').slice(-1)[0]

      Vue.http.get(url).then((res) => {
        // deklarasikan parse body json, untuk memparse isi dari json
        let parseBody = (val) => {
          try {
            let body = Hjson.parse(val)
            if (body) {
              res.body = body
              compileComponent(res.body)
            }
          } catch (err) {
            console.group('Invalid ' + formatComponentName(componentName) + ' component: ')
            let pos = err.message.substr(err.message.indexOf('at position') + 12).split(' ')[0]
            var str = val.substr(pos - 10, 20).replace(/[\n\t]/g, ' ') + '\n         ^'
            if (pos < 10) {
              str = val.substr(pos - 3, 20).replace(/[\n\t]/g, ' ') + '\n  ^'
            } else if (pos > 50) {
              str = val.substr(pos - 25, 50).replace(/[\n\t]/g, ' ') + '\n                        ^'
            }
            console.log(str)
            console.error(err)
            console.groupEnd()
          }
        }

        // compile component
        let compileComponent = (jsonBody) => {
          this.initContext(componentName, jsonBody)
            .then(res => { return this.loadLibraries() })
            .then(res => { return this.createComponent(isRoot) })
            .then(res => {
              resolve(res)
            })
        }

        // component is in plain old json format
        // if (!res.body && res.bodyText) {
        //   res.bodyText.then(val => {
        //     parseBody(val)
        //   })
        //   resolve(false)
        //   return false
        // } else if (res.body.constructor === window.Blob) {
        //   let reader = new window.FileReader()
        //   reader.readAsText(res.body)
        //   reader.onloadend = () => {
        //     parseBody(reader.result)
        //   }
        // }

        // component is in hjson format
        if (typeof res.body === 'string') {
          parseBody(res.body)
        } else {
          compileComponent(res.body)
        }
      }, (res) => {
        // kalau gagal mengambil file nya maka resolve dengan gagal
        resolve(false)
      })
    })
  }

  initContext (componentName, json) {
    let context = JSON.parse(JSON.stringify(COMPONENT_DEFAULT_CONTEXT))
    this.context = context
    return new Promise(resolve => { // eslint-disable-line
      _.assignIn(context, json)
      context.name = componentName

      context.ctrl.url = componentName => {
        return COMPONENT_CTRL.replace('{component}', componentName)
      }

      if (!json.template) {
        Vue.http.get(this.templateUrl).then(response => {
          context.template = response.body.split('<!--END_OF_TEMPLATE-->')[0]
          resolve()
        }).catch((e) => {
          printError('Failed to get templateFile: ' + this.templateUrl, {
            component: componentName
          })
        })
      } else {
        resolve()
      }
    })
  }

  loadLibraries () {
    let context = this.context
    return new Promise(resolve => {
      // determine if we should load external libraries
      let libs = context.require.libs || []
      var shouldLoadLibs = false
      let defaultLibs = []
      _.map(libs, (lib, idx) => {
        if (_.isString(lib)) {
          defaultLibs.push(lib)
          shouldLoadLibs = true
        } else if (_.isArray(lib)) {
          if (!context.libs[lib[0]]) {
            context.libs[lib[0]] = []
            shouldLoadLibs = true
          }
        }
      })

      // filter default libs
      libs = _.filter(libs, lib => !_.isString(lib))
      if (defaultLibs.length > 0) {
        var defaultLibsFound = false
        _.map(libs, lib => {
          if (lib[0] === 'default') {
            _.map(lib[1], lib => {
              libs.default[1].push(lib)
            })
            defaultLibsFound = true
          }
        })

        if (!defaultLibsFound) {
          libs.push(['default', defaultLibs])
        }
      }

      if (shouldLoadLibs) {
        // convert each file path to correct path
        _.map(libs, lib => {
          context.libs[lib[0]] = {
            all: lib[1],
            loaded: []
          }

          lib[1].forEach((file, key) => {
            lib[1][key] = COMPONENT_LIB_PATH
            lib[1][key] = lib[1][key].replace('{component}', this.componentPath)
            if (lib[0] === 'default') {
              lib[1][key] = lib[1][key].replace('/{lib}', '')
            } else {
              lib[1][key] = lib[1][key].replace('{lib}', lib[0])
            }
            lib[1][key] = lib[1][key].replace('{file}', file)
          })
        })

        // load all libraries dependencies
        loadlibs(context, libs, (lib, file) => {
          context.libs[lib].loaded.push(file)
        }, () => {
          resolve()
        })
      } else {
        // we dont need to load external libs
        resolve()
      }
    })
  }

  createComponent (isRoot) {
    // prepare params
    let context = this.context
    let initParams = {
      template: context.template
    }
    this.initParams = initParams

    // parse array to a function
    let parseFuncArr = (funcarr) => {
      var funcstr = ''
      if (_.isArray(funcarr)) {
        funcstr = funcarr.join('\n')
      } else {
        funcstr = funcarr
      }

      if (funcstr.trim() === '') {
        return 'function() {};'
      }
      return funcstr
    }

    return new Promise(resolve => {
      // define illegal varname
      let illegalVarName = ['context', 'this', 'base_url']
      let isVarValid = (obj, location, vars) => {
        var valid = true
        vars.forEach(varname => {
          if (typeof obj[varname] !== 'undefined') {
            printError('Local variable name can not be `' + varname + '` ', {
              component: context.name,
              location: location + '.' + varname,
              hint: 'Please change `' + varname + '` variable to another name'
            })
            valid = false
          }
        })

        return valid
      }

      // bind context data
      initParams.data = () => {
        let data = _.isObject(context.data) ? context.data : {}
        if (isRoot) {
          let parseVuexModules = (root, module) => {
            if (typeof root === 'undefined') {
              return {}
            }

            let stores = {
              state: _.isObject(root.state) ? root.state : {},
              getters: {},
              mutations: {},
              actions: {},
              modules: {}
            }

            _.each(root, (_store, key) => {
              if (['getters', 'mutations', 'actions'].indexOf(key) >= 0) {
                let prefix = module ? module + '.' : ''
                _.map(_store, (funcarr, funcname) => {
                  let funcstr = parseFuncArr(funcarr)
                  eval('stores.' + key + '["' + prefix + funcname + '"] = ' + funcstr) // eslint-disable-line
                })
              }

              if (_.isObject(_store)) {
                let store = parseVuexModules(_store, key)
                stores.modules[key] = store
              }
            })

            return stores
          }
          Component.store = new Vuex.Store(parseVuexModules(context.store))
          Component.store.strict = process.env.NODE_ENV !== 'production'
        }

        data.store = Component.store

        return data
      }

      // bind context props
      if (!_.isEmpty(context.props)) {
        initParams.props = context.props

        if (!isVarValid(context.props, 'context.props', illegalVarName)) {
          return false
        }
      }

      // bind context watch
      initParams.watch = {}
      if (!_.isEmpty(context.watch) && _.isObject(context.watch)) {
        if (!isVarValid(context.watch, 'context.watch', illegalVarName)) {
          return false
        }

        _.map(context.watch, (funcarr, funcname) => {
          let funcstr = parseFuncArr(funcarr)
          eval('initParams.watch[funcname] = ' + funcstr) // eslint-disable-line
        })
      }

      // bind context computed
      initParams.computed = {}
      if (!_.isEmpty(context.computed) && _.isObject(context.computed)) {
        if (!isVarValid(context.computed, 'context.computed', illegalVarName)) {
          return false
        }

        _.map(context.computed, (funcarr, funcname) => {
          let funcstr = parseFuncArr(funcarr)
          eval('initParams.computed[funcname] = ' + funcstr) // eslint-disable-line
        })
      }

      // parse function in data
      let computedData = {}
      let parseComputed = (state, key) => {
        if (_.isObject(state)) {
          _.each(state, (v, k) => {
            if (k[0] === '@') {
              let funcstr = parseFuncArr(v)
              computedData[key + '.' + k.substr(1)] = funcstr
              delete state[k]
            } else if (_.isObject(v)) {
              parseComputed(v, typeof key !== 'undefined' ? key + '.' + k : k)
            }
          })
        }
        return state
      }

      // execute parse computed data
      parseComputed(context.data)
      if (!_.isEmpty(computedData)) {
        var idx = 1
        _.each(computedData, (v, k) => {
          eval("initParams.computed['_cdata" + (idx) + "'] = " + v) // eslint-disable-line

          // we should watch when _cdata changed, because
          // it will lose ref when changed
          initParams.watch['_cdata' + idx] = function (n) {
            let a = new Function('n','this.' + k + ' = n') // eslint-disable-line
            a.bind(this)(n)
          }
          idx++
        })
      }

      // prepare params
      initParams.computed.base_url = () => {
        return COMPONENT_ROOT
      }
      initParams.computed.context = () => {
        return context
      }
      if (_.isObject(context.api)) {
        initParams.computed.api = function () {
          if (!context.apiCompiled) {
            context.apiCompiled = {}
            _.each(context.api, (api, apiname) => {
              let apisplit = apiname.split('#')
              let name = apisplit[0]
              let verb = apisplit[1] || 'get'
              context.apiCompiled[name] = (params) => {
                let url = api.url.replace(/\{([^}]+)\}/ig, p => {
                  let par = p.substr(1, p.length - 2)
                  let paramVal = new Function("return this." + par)  // eslint-disable-line
                  return paramVal.apply(this)
                })
                return new Promise((resolve, reject) => {
                  Vue.http[verb](url)
                  .then(res => {
                    if (api.success) {
                      eval('let ret = ' + parseFuncArr(api.success) + '; resolve(ret(res))') // eslint-disable-line
                    } else {
                      eval('resolve(res)') // eslint-disable-line
                    }
                  })
                  .catch(res => {
                    if (api.error) {
                      eval('return ' + parseFuncArr(api.error)) // eslint-disable-line
                    }
                  })
                })
              }
            })
          }

          return context.apiCompiled
        }
      }

      // bind context method
      if (!_.isEmpty(context.methods) && _.isObject(context.methods)) {
        if (!isVarValid(context.methods, 'context.methods', illegalVarName)) {
          return false
        }
        initParams.methods = {}

        _.map(context.methods, (funcarr, funcname) => {
          let funcstr = parseFuncArr(funcarr)
          eval('initParams.methods[funcname] = ' + funcstr) // eslint-disable-line
        })
      }

      // bind context events
      if (!_.isEmpty(context.events) && _.isObject(context.events)) {
        _.map(context.events, (funcarr, funcname) => {
          let funcstr = parseFuncArr(funcarr)
          eval('initParams[funcname] = ' + funcstr) // eslint-disable-line
        })
      }

      // put current vm onto context's vms list
      let onBeforeCreate = initParams.beforeCreate || new Function() // eslint-disable-line
      initParams.beforeCreate = function () {
        Component.vms.byUID[this._uid] = this
        if (!_.isArray(Component.vms.byName[this.$options._componentTag])) {
          Component.vms.byName[this.$options._componentTag] = []
        }
        Component.vms.byName[this.$options._componentTag].push(this)
        context.vms = Component.vms
        context.defs = Component.defs
        onBeforeCreate.apply(this)
      }

      let onCreated = initParams.created || new Function() // eslint-disable-line
      initParams.created = function () {
        var idx = 1
        _.each(computedData, (v, k) => {
          let a = new Function('this.' + k + ' = this._cdata' + (idx++)) // eslint-disable-line
          a.apply(this)
        })
        onCreated.apply(this)
      }

      // if there are required component
      if (context.require.components && context.require.components.length > 0) {
        // then load current component dependencies
        var componentLoader = false
        context.require.components.forEach((com, idx) => {
          if (!componentLoader) {
            componentLoader = new Component(com)
          } else {
            componentLoader = componentLoader.then(result => {
              return new Component(com)
            })
          }
        })

        // after all dependencies loaded, then load current component
        componentLoader.then(result => {
          if (!Component.defs[context.name]) {
            Component.defs[context.name] = Vue.component(context.name, initParams)
          }
          resolve(context)
        })
      } else {
        // else, we dont have any dependencies, just load current component
        if (!Component.defs[context.name]) {
          Component.defs[context.name] = Vue.component(context.name, initParams)
        }
        resolve(context)
      }
    })
  }
}
