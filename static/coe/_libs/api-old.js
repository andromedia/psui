/* eslint-disable */

var siteUrl = "http://coe.semenindonesia.com/DocumentManagement";
var dao = new SPScript.RestDao(siteUrl);
var wp = dao.lists("Work Packages")
var wpi = dao.lists("Work Packages Item")

getWPI = function(self) {
	wpGetItems().then(function(res) {
		res.forEach(function(r) {
			self.state.page.wpmaster[r.Id] = {
				id: r.Id,
				wpname: r.Title,
				pic: r.PIC,
				project: r.Project_x0020_Name.Title,
				pid: r.Project_x0020_NameId
			}
		})

		wpiGetItems().then(function(wres) {
			var tgl = null

			wres.forEach(function(w) {
				tgl = w.Tgl
				if (!self.state.page.wpi[tgl]) {
					self.state.page.wpi[tgl] = []
				}
				var pid = self.state.page.wpmaster[w.Work_x0020_PackageId].id
				var val = {
					PID: self.state.page.wpmaster[w.Work_x0020_PackageId].id,
					WPID: self.state.page.wpmaster[w.Work_x0020_PackageId].pid,
					Project: self.state.page.wpmaster[w.Work_x0020_PackageId].project,
					Work: self.state.page.wpmaster[w.Work_x0020_PackageId].wpname,
					EngPlan: w.EngPlan,
					EngActual: w.EngActual,
					Fs: w.Fs,
					Pr: w.Pr,
					Po: w.Po,
					Eac: w.Eac,
					ProcPlan: w.ProcPlan,
					ProcActual: w.ProcActual,
					ComPlan: w.ComPlan,
					ComActual: w.ComActual,
					ConPlan: w.ConPlan,
					ConActual: w.ConActual
				}
				self.state.page.wpi[tgl].push(val)

				// add wpip
				if (!self.state.page.wpip[tgl]) {
					self.state.page.wpip[tgl] = []
				}
				if (!self.state.page.wpip[tgl][pid]) {	
					self.state.page.wpip[tgl][pid] = []
				}
				self.state.page.wpip[tgl][pid].push(val)
				self.tgl = tgl
				self.tgldisplay = moment(tgl).format('DD MMM')
			})

			drawChart(self)
		})
	})
}

wpGetItems = function() {
	// return new Promise(function(resolve, reject) {
	// 	resolve([{
	// 		Id: 1,
	// 		Title: "Test",
	// 		PIC: "rizky",
	// 		Project_x0020_Name: {
	// 			"Title": "Project1"
	// 		},
	// 		Project_x0020_NameId: 1
	// 	},{
	// 		Id: 2,
	// 		Title: "Test 2",
	// 		PIC: "rizky",
	// 		Project_x0020_Name: {
	// 			"Title": "Project2"
	// 		},
	// 		Project_x0020_NameId: 1
	// 	}])
	// })
	return wp.getItems('$select=Id,Project_x0020_Name/Title, Title, PIC&$expand=Project_x0020_Name')
}

wpiGetItems = function() {
	// return new Promise(function(resolve, reject) {
	// 	resolve([
	// 	/**************** WP 1 ********************/
	// 	{
	// 		Work_x0020_PackageId: 1,
	// 		Tgl: "2016-11-01",
	// 		EngPlan: 10,
	// 		EngActual: 5,
	// 		Fs: 1,
	// 		Pr: 2,
	// 		Po: 3,
	// 		Eac: 4,
	// 		ProcPlan: 20,
	// 		ProcActual: 30,
	// 		ComPlan: 30,
	// 		ComActual: 40,
	// 		ConPlan: 40,
	// 		ConActual: 50
	// 	},{
	// 		Work_x0020_PackageId: 1,
	// 		Tgl: "2016-11-02",
	// 		EngPlan: 15,
	// 		EngActual: 8,
	// 		Fs: 1,
	// 		Pr: 2,
	// 		Po: 3,
	// 		Eac: 4,
	// 		ProcPlan: 15,
	// 		ProcActual: 20,
	// 		ComPlan: 20,
	// 		ComActual: 30,
	// 		ConPlan: 30,
	// 		ConActual: 40
	// 	},{
	// 		Work_x0020_PackageId: 1,
	// 		Tgl: "2016-11-07",
	// 		EngPlan: 20,
	// 		EngActual: 15,
	// 		Fs: 1,
	// 		Pr: 2,
	// 		Po: 3,
	// 		Eac: 4,
	// 		ProcPlan: 15,
	// 		ProcActual: 20,
	// 		ComPlan: 20,
	// 		ComActual: 30,
	// 		ConPlan: 30,
	// 		ConActual: 40
	// 	},{
	// 		Work_x0020_PackageId: 1,
	// 		Tgl: "2016-11-09",
	// 		EngPlan: 35,
	// 		EngActual: 20,
	// 		Fs: 1,
	// 		Pr: 2,
	// 		Po: 3,
	// 		Eac: 4,
	// 		ProcPlan: 15,
	// 		ProcActual: 20,
	// 		ComPlan: 20,
	// 		ComActual: 30,
	// 		ConPlan: 30,
	// 		ConActual: 40
	// 	},{
	// 		Work_x0020_PackageId: 1,
	// 		Tgl: "2016-11-12",
	// 		EngPlan: 40,
	// 		EngActual: 0,
	// 		Fs: 1,
	// 		Pr: 2,
	// 		Po: 3,
	// 		Eac: 4,
	// 		ProcPlan: 15,
	// 		ProcActual: 20,
	// 		ComPlan: 20,
	// 		ComActual: 30,
	// 		ConPlan: 30,
	// 		ConActual: 40
	// 	},{
	// 		Work_x0020_PackageId: 1,
	// 		Tgl: "2016-11-16",
	// 		EngPlan: 45,
	// 		EngActual: 0,
	// 		Fs: 1,
	// 		Pr: 2,
	// 		Po: 3,
	// 		Eac: 4,
	// 		ProcPlan: 15,
	// 		ProcActual: 20,
	// 		ComPlan: 20,
	// 		ComActual: 30,
	// 		ConPlan: 30,
	// 		ConActual: 40
	// 	},{
	// 		Work_x0020_PackageId: 1,
	// 		Tgl: "2016-11-19",
	// 		EngPlan: 50,
	// 		EngActual: 0,
	// 		Fs: 1,
	// 		Pr: 2,
	// 		Po: 3,
	// 		Eac: 4,
	// 		ProcPlan: 15,
	// 		ProcActual: 20,
	// 		ComPlan: 20,
	// 		ComActual: 30,
	// 		ConPlan: 30,
	// 		ConActual: 40
	// 	}

	// 	/**************** WP 2 ********************/
	// 	,{
	// 		Work_x0020_PackageId: 2,
	// 		Tgl: "2016-11-01",
	// 		EngPlan: 10,
	// 		EngActual: 5,
	// 		Fs: 1,
	// 		Pr: 2,
	// 		Po: 3,
	// 		Eac: 4,
	// 		ProcPlan: 20,
	// 		ProcActual: 30,
	// 		ComPlan: 30,
	// 		ComActual: 40,
	// 		ConPlan: 40,
	// 		ConActual: 50
	// 	},{
	// 		Work_x0020_PackageId: 2,
	// 		Tgl: "2016-11-02",
	// 		EngPlan: 15,
	// 		EngActual: 10,
	// 		Fs: 1,
	// 		Pr: 2,
	// 		Po: 3,
	// 		Eac: 4,
	// 		ProcPlan: 15,
	// 		ProcActual: 20,
	// 		ComPlan: 20,
	// 		ComActual: 30,
	// 		ConPlan: 30,
	// 		ConActual: 40
	// 	}])
	// })
	return wpi.getItems()
}

calculateWP = function(self, wpraw, tgl) {
	self.tgl = tgl;
	var wp = wpraw[tgl]
	var raw = {};
	for (var w in wp) {
		if (!raw[wp[w].WPID]) {
			raw[wp[w].WPID] = []
		}

		raw[wp[w].WPID].push(wp[w])
	}

	var result = {}
	for (var r in raw) {
		for (var k in raw[r]) {
			var pid = raw[r][k].PID
			result[raw[r][k].PID] = result[raw[r][k].PID] || {
				PID: raw[r][k].PID,
				WPID: raw[r][k].WPID,
				Project: raw[r][k].Project,
				EngPlan: 0,
				EngActual: 0,
				Fs: 0,
				Pr: 0,
				Po: 0,
				Eac: 0,
				ProcPlan: 0,
				ProcActual: 0,
				ComPlan: 0,
				ComActual: 0,
				ConPlan: 0,
				ConActual: 0,
				Count: 0
			}

			result[pid].EngPlan += raw[r][k].EngPlan * 1 
			result[pid].EngActual += raw[r][k].EngActual  * 1
			result[pid].Fs += raw[r][k].Fs  * 1
			result[pid].Pr += raw[r][k].Pr  * 1
			result[pid].Po += raw[r][k].Po  * 1
			result[pid].Eac += raw[r][k].Eac  * 1
			result[pid].ProcPlan += raw[r][k].ProcPlan  * 1
			result[pid].ProcActual += raw[r][k].ProcActual  * 1
			result[pid].ComPlan += raw[r][k].ComPlan  * 1
			result[pid].ComActual += raw[r][k].ComActual  * 1
			result[pid].ConPlan += raw[r][k].ConPlan  * 1
			result[pid].ConActual += raw[r][k].ConActual  * 1
			result[pid].Count += 1
		}
	}

	var res = []
	for (var i in result) {
		var c = result[i].Count
		result[i].EngPlan /= c
		result[i].EngActual /= c
		result[i].Fs /= c
		result[i].Pr /= c
		result[i].Po /= c
		result[i].Eac /= c
		result[i].ProcPlan /= c
		result[i].ProcActual /= c
		result[i].ComPlan /= c
		result[i].ComActual /= c
		result[i].ConPlan /= c
		result[i].ConActual /= c
		res.push(result[i])
	}
	return res
}

window.chart = null
drawChart = function(self) {
	// calculate table
	var calculateTable = function() {
		var categories = [] 
		var result = [];
		var plan = []
		var actual = []
		var todayIdx = 0;

		if (!self.project) {
			self.current = calculateWP(self, self.state.page.wpi, self.tgl)
		} else {
			self.current = self.state.page.wpip[self.tgl][self.project.PID]	
		}

		// calculate total: start
		for (var tgl in self.state.page.wpi) {
			categories.push(tgl)
		}
		categories.sort()
		for (var i in categories) {
			var tgl = categories[i]
			var resday = {
				EngPlan: 0,
				EngActual: 0,
				Fs: 0,
				Pr: 0,
				Po: 0,
				Eac: 0,
				ProcPlan: 0,
				ProcActual: 0,
				ComPlan: 0,
				ComActual: 0,
				ConPlan: 0,
				ConActual: 0,
				Count: 0
			}

			var resarr = !self.project ? self.state.page.wpi[tgl] : self.state.page.wpip[tgl][self.project.PID]	;
			for (var idx in resarr) {
				var item = resarr[idx]
				resday.EngPlan += item.EngPlan * 1 
				resday.EngActual += item.EngActual  * 1
				resday.Fs += item.Fs  * 1
				resday.Pr += item.Pr  * 1
				resday.Po += item.Po  * 1
				resday.Eac += item.Eac  * 1
				resday.ProcPlan += item.ProcPlan  * 1
				resday.ProcActual += item.ProcActual  * 1
				resday.ComPlan += item.ComPlan  * 1
				resday.ComActual += item.ComActual  * 1
				resday.ConPlan += item.ConPlan  * 1
				resday.ConActual += item.ConActual  * 1
				resday.Count += 1
			}
			var c = resday.Count
			resday.EngPlan = Math.round(resday.EngPlan / c)
			resday.EngActual = Math.round(resday.EngActual / c)
			// resday.Fs /= c
			// resday.Pr /= c
			// resday.Po /= c
			// resday.Eac /= c
			resday.ProcPlan = Math.round(resday.ProcPlan / c)
			resday.ProcActual = Math.round(resday.ProcActual / c)
			resday.ComPlan = Math.round(resday.ComPlan / c)
			resday.ComActual = Math.round(resday.ComActual / c)
			resday.ConPlan = Math.round(resday.ConPlan / c)
			resday.ConActual = Math.round(resday.ConActual / c)

			result.push(resday)
			if (tgl === self.tgl) {
				self.total = resday;
				todayIdx = i;
			}
		}	

		switch (self.state.page.phase) {
			case "Preparation":
			break;
			case "Engineering":
				self.sidebar = {
					actual: result[todayIdx].EngActual,
					plan: result[todayIdx].EngPlan
				} 
			break;
			case "Procurement":
				self.sidebar = {
					actual: result[todayIdx].ProcActual,
					plan: result[todayIdx].ProcPlan
				} 
			break;
			case "Construction":
				self.sidebar = {
					actual: result[todayIdx].ConActual,
					plan: result[todayIdx].ConPlan
				} 
			break;
			case "Commisioning":
				self.sidebar = {
					actual: result[todayIdx].ComActual,
					plan: result[todayIdx].ComPlan
				} 
			break;
			case "Closing Project":
			break;
		}

		return [result, categories]
	}

	var res = calculateTable();
	var result = res[0];
	var categories = res[1];
	var plan = [];
	var actual = [];

	for (var i in result) {
		var r = result[i]
		switch (self.state.page.phase) {
			case "Preparation":
			break;
			case "Engineering":
				actual.push(r.EngActual);
				plan.push(r.EngPlan);
			break;
			case "Procurement":
				actual.push(r.ProcActual);
				plan.push(r.ProcPlan);
			break;
			case "Construction":
				actual.push(r.ConActual);
				plan.push(r.ConPlan);
			break;
			case "Commisioning":
				actual.push(r.ComActual);
				plan.push(r.ComPlan);
			break;
			case "Closing Project":
			break;
		}
	}

	if (window.chart) {
		window.chart.destroy()
	}

	var catidx = []
	for (var i in categories) {
		categories[i] = moment(categories[i]).format('DD MMM')
		catidx[categories[i]] = i;
	}


	window.chart = Highcharts.chart('container', {
        title: {
            text: 'Progress Pengerjaan',
            x: -20 //center
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y}%</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        xAxis: {
            categories: categories,
            labels: {
                formatter: function () {
                	if (self.tgldisplay === this.value) {
                        return '<span class="selected-tgl-xaxis">' + this.value + '</span>';
                    } else {
                        return '<span class="unselected-tgl-xaxis">' + this.value + '</span>';
                    }
                }
            }
        },
        yAxis: {
            title: {
                text: 'Progress Pengerjaan (%)'
            }
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
        series: [{
            name: 'Plan',
            data: plan
        }, {
            name: 'Actual',
            data: actual
        }]
    },function (chart) {
	    $('.highcharts-xaxis-labels text').on('click', function () {
	    	self.tgldisplay = $(this).text();
	    	self.tgl = categories[catidx[self.tgldisplay]];

	    	$(".selected-tgl-xaxis").removeClass("selected-tgl-xaxis").addClass("unselected-tgl-xaxis");
	    	$(this).find('.unselected-tgl-xaxis').removeClass("unselected-tgl-xaxis").addClass("selected-tgl-xaxis");
	    	calculateTable()
		});
    });

}